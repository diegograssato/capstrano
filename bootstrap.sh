#!/bin/bash

echo ">>> Cria SWAP"
sudo dd if=/dev/zero of=/swapfile bs=2048 count=512k
sudo mkswap /swapfile
sudo swapon /swapfile
sudo echo "/swapfile       none    swap    sw      0       0 " >> /etc/fstab
sudo chown vagrant:vagrant /swapfile
sudo chmod 0600 /swapfile

clear
sudo cp -p /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
echo "America/Sao_Paulo" | sudo tee /etc/timezone
## Locale do sistema
echo -e "en_US.UTF-8 UTF-8\npt_BR ISO-8859-1\npt_BR.UTF-8 UTF-8" | sudo tee /var/lib/locales/supported.d/local
sudo dpkg-reconfigure locales


# Define diretivas que permitirão instalar MySQL sem perguntar senha
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'


# Atualiza lista de pacotes
sudo apt-get update
sudo  apt-get install -y \
build-essential \
g++ \
make \
vim \
curl \
wget \
ruby1.9.1-full \
nodejs \
git-core \
npm

sudo apt-get remove node -y
sudo apt-get autoremove -y
sudo npm install bower grunt-cli -g -s
sudo ln -s /usr/bin/nodejs /usr/bin/node

sudo gem install capistrano


